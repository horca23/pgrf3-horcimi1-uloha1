#version 150

in vec3 color;
in vec4 depthTexCoord;
in vec2 texCoord;

in vec3 normal;
in vec3 light;
in vec3 viewDirection;
in vec3 NdotL;

in float intensity;

uniform  int lighting;
uniform int showNormals;
uniform int transformation;
uniform int obj;

uniform sampler2D textureID;
uniform sampler2D depthTexture;

vec4 calculatePerVertex() {
    vec4 color;
    if (intensity>0.95) color=vec4(1.0, 0.5, 0.5, 1.0);
    else if (intensity>0.8) color=vec4(0.6, 0.3, 0.3, 1.0);
    else if (intensity>0.5) color=vec4(0.0, 0.0, 0.3, 1.0);
    else if (intensity>0.25) color=vec4(0.4, 0.2, 0.2, 1.0);
    else color=vec4(0.2, 0.1, 0.1, 1.0);
    return vec4(color);
}

vec4 calculatePerPixel() {
    float pixelIntensity = dot(normalize(light), normalize(normal));
    vec4 color;
    if (pixelIntensity>0.95) color=vec4(1.0, 0.5, 0.5, 1.0);
    else if (pixelIntensity>0.8) color=vec4(0.6, 0.3, 0.3, 1.0);
    else if (pixelIntensity>0.5) color=vec4(0.0, 0.0, 0.3, 1.0);
    else if (pixelIntensity>0.25) color=vec4(0.4, 0.2, 0.2, 1.0);
    else color=vec4(0.2, 0.1, 0.1, 1.0);
    return vec4(color);
}

vec4 calculateBlinnPhong() {
    vec4 ambient = vec4(0.4, 0.0, 0.4, 1.0);
    vec4 diffuse = vec4(normalize(NdotL) * vec3(0.2, 0.2, 0.9), 1.0);
    vec3 halfVector = normalize(normalize(light) + normalize(viewDirection));
    float NdotH = max(0, dot(normalize(normal), halfVector));
    vec4 specular = vec4(pow(NdotH, 16) * vec4(0.0, 0.0, 1.0, 1.0));
    vec4 color = ambient + diffuse + specular;
    vec4 texColor = texture(textureID, texCoord);
    float z1 = texture(depthTexture, depthTexCoord.xy / depthTexCoord.w).r;
    float z2 = depthTexCoord.z / depthTexCoord.w;
    bool shadow = z1 < z2 - 0.0001;

    if (shadow) {
        return texColor * ambient;
    } else {
        return texColor * color;
    }
}

void main() {
    if (obj == 2) { // drawing light source
        gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    } else if (showNormals == 1) {
        gl_FragColor = vec4(normalize(normal), 1.0);
    } else {
        switch (transformation) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                gl_FragColor = vec4(color, 1.0);
                break;
            case 8:
            case 9:
                switch (lighting) {
                    case 0:
                        gl_FragColor = calculatePerVertex();
                        break;
                    case 1:
                        gl_FragColor = calculatePerPixel();
                        break;
                    case 2:
                        gl_FragColor = calculateBlinnPhong();
                        break;
                }
                break;
        }
    }
}