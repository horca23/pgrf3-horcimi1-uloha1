#version 150

in vec2 inPosition;

uniform mat4 view;
uniform mat4 proj;
uniform int transformation;
uniform float time;
uniform int coverMovement;

uniform vec3 lightPosition;
uniform mat4 lightMat;

uniform int obj;

out vec3 color;
out vec4 depthTexCoord;
out vec2 texCoord;
out vec3 normal;
out vec3 light;
out vec3 viewDirection;
out vec3 NdotL;

out float intensity;

const float PI = 3.14159265359;

mat3 translate(float x, float y) {
    return mat3(
        1, 0, x,
        0, 1, y,
        0, 0, 1
    );
}
mat4 translate(float x, float y, float z) {
    return mat4(
        1, 0, 0, x,
        0, 1, 0, y,
        0, 0, 1, z,
        0, 0, 0, 1
    );
}
mat3 rotate(float angle) {
    return mat3(
        cos(angle), -sin(angle), 0,
        sin(angle), cos(angle), 0,
        0, 0, 1
    );
}
mat3 scale(float x, float y) {
    return mat3(
        x, 0, 0,
        0, y, 0,
        0, 0, 1
    );
}

vec3 transformCartesian(vec2 xy) {
    float s = (xy.x * 2 - 1) * PI;
    float t = (xy.y * 2 - 1) * PI/2;

    float x = cos(s) * cos(t);
    float y = 2 * sin(s) * cos(t);
    float z = 0.5 * sin(t);

    return vec3(x, y, z);
}
vec3 cartesianNormal(vec2 xy) {
    vec3 u = transformCartesian(xy + vec2(0.001, 0)) - transformCartesian(xy - vec2(0.001, 0));
    vec3 v = transformCartesian(xy + vec2(0, 0.001)) - transformCartesian(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformCartesianCustom(vec2 xy) {
    float s = xy.x * 4 - 2;
    float t = xy.y * 4 - 2;

    float x = s * cos(t);
    float y = s * sin(t);
    float z = t;

    return vec3(x, y, z);
}
vec3 cartesianCustomNormal(vec2 xy) {
    vec3 u = transformCartesianCustom(xy + vec2(0.001, 0)) - transformCartesianCustom(xy - vec2(0.001, 0));
    vec3 v = transformCartesianCustom(xy + vec2(0, 0.001)) - transformCartesianCustom(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformSpherical(vec2 xy) {
    float s = xy.x * 2 * PI;
    float t = xy.y * PI;

    float rho = 1 + 0.2 * sin(6 * s) * sin(5 * t);
    float phi = t;
    float theta = s;

    float x = rho * sin(phi) * cos(theta);
    float y = rho * sin(phi) * sin(theta);
    float z = rho * cos(phi);

    return vec3(x, y, z);
}
vec3 sphericalNormal(vec2 xy) {
    vec3 u = transformSpherical(xy + vec2(0.001, 0)) - transformSpherical(xy - vec2(0.001, 0));
    vec3 v = transformSpherical(xy + vec2(0, 0.001)) - transformSpherical(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformSphericalCustom(vec2 xy) {
    float s = xy.x * 2 * PI;
    float t = xy.y * PI;

    float rho = sin(2 * s) * sin(3 * t);
    float phi = t;
    float theta = s;

    float x = rho * sin(phi) * cos(theta);
    float y = rho * sin(phi) * sin(theta);
    float z = rho * cos(phi);

    return vec3(x, y, z);
}
vec3 sphericalCustomNormal(vec2 xy) {
    vec3 u = transformSphericalCustom(xy + vec2(0.001, 0)) - transformSphericalCustom(xy - vec2(0.001, 0));
    vec3 v = transformSphericalCustom(xy + vec2(0, 0.001)) - transformSphericalCustom(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformCylindrical(vec2 xy) {
    float s = xy.x * 2 * PI;
    float t = xy.y * 2 * PI;

    float r = (1 + max(sin(t), 0)) * 0.5 * t;
    float theta = s;
    float zz = 3 - t;

    float x = r * cos(theta);
    float y = r * sin(theta);
    float z = zz;

    return vec3(x, y, z);
}
vec3 cylindricalNormal(vec2 xy) {
    vec3 u = transformCylindrical(xy + vec2(0.001, 0)) - transformCylindrical(xy - vec2(0.001, 0));
    vec3 v = transformCylindrical(xy + vec2(0, 0.001)) - transformCylindrical(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformCylindricalCustom(vec2 xy) {
    float s = xy.x * 2 * PI - PI;
    float t = xy.y * 2 * PI - PI;

    float r = cos(t) * sin(t);
    float theta = s;
    float zz = t;

    float x = r * cos(theta);
    float y = r * sin(theta);
    float z = zz;

    return vec3(x, y, z);
}
vec3 cylindricalCustomNormal(vec2 xy) {
    vec3 u = transformCylindricalCustom(xy + vec2(0.001, 0)) - transformCylindricalCustom(xy - vec2(0.001, 0));
    vec3 v = transformCylindricalCustom(xy + vec2(0, 0.001)) - transformCylindricalCustom(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformWithTime(vec2 xy) {
    float h = sin(xy.y * 10 + time * 10) / 10;
    return vec3(xy.x, xy.y, h);
}
vec3 timeNormal(vec2 xy) {
    vec3 u = transformWithTime(xy + vec2(0.001, 0)) - transformWithTime(xy - vec2(0.001, 0));
    vec3 v = transformWithTime(xy + vec2(0, 0.001)) - transformWithTime(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformCover(vec2 xy) {
    vec3 transformed;
    if (coverMovement == 0) {
        transformed = vec3(xy, 0.0);
    } else if (coverMovement == 1) {
        transformed = vec3(xy, 1.0) * translate(-0.5, -0.5) * rotate(time) * translate(0.5, 0.5) * translate(0, sin(2 * time) * 2);
    }
    return vec3(transformed.x, transformed.y, 2.0);
}
vec3 transformCoverNormal(vec2 xy) {
    vec3 u = transformCover(xy + vec2(0.001, 0)) - transformCover(xy - vec2(0.001, 0));
    vec3 v = transformCover(xy + vec2(0, 0.001)) - transformCover(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 scaleObject(vec2 xy) {
    vec3 scaled = vec3(xy, 1.0) * scale(4, 4) * translate(-2, -2);
    return vec3(scaled.xy, 0.0);
}
vec3 scaleObjectNormal(vec2 xy) {
    vec3 u = scaleObject(xy + vec2(0.001, 0)) - scaleObject(xy - vec2(0.001, 0));
    vec3 v = scaleObject(xy + vec2(0, 0.001)) - scaleObject(xy - vec2(0, 0.001));
    return cross(u, v);
}

vec3 transformLightSource() {
    float s = inPosition.x * 2 * PI;
    float t = inPosition.y * PI;

    float x = sin(t) * cos(s);
    float y = sin(t) * sin(s);
    float z = cos(t);

    return vec3(x / 4 + lightPosition.x, y / 4 + lightPosition.y, z / 4 + lightPosition.z);
}

vec3 noTransformation(vec2 xy) {
    return vec3(xy, 0.0);
}
vec3 noTransformationNormal(vec2 xy) {
    vec3 u = noTransformation(xy + vec2(0.001, 0)) - noTransformation(xy - vec2(0.001, 0));
    vec3 v = noTransformation(xy + vec2(0, 0.001)) - noTransformation(xy - vec2(0, 0.001));
    return cross(u, v);
}

void main() {
    vec3 xyz;
    if (obj == 0) {
        switch (transformation) {
            case 0:
                xyz = noTransformation(inPosition);
                normal = noTransformationNormal(inPosition);
                break;
            case 1:
            case 9:
                xyz = transformCartesian(inPosition);
                normal = cartesianNormal(inPosition);
                break;
            case 2:
                xyz = transformCartesianCustom(inPosition);
                normal = cartesianCustomNormal(inPosition);
                break;
            case 3:
                xyz = transformSpherical(inPosition);
                normal = sphericalNormal(inPosition);
                break;
            case 4:
                xyz = transformSphericalCustom(inPosition);
                normal = sphericalCustomNormal(inPosition);
                break;
            case 5:
                xyz = transformCylindrical(inPosition);
                normal = cylindricalNormal(inPosition);
                break;
            case 6:
                xyz = transformCylindricalCustom(inPosition);
                normal = cylindricalCustomNormal(inPosition);
                break;
            case 7:
                xyz = transformWithTime(inPosition);
                normal = timeNormal(inPosition);
                break;
            case 8:
                xyz = scaleObject(inPosition);
                normal = scaleObjectNormal(inPosition);
                break;
            default :
                xyz = noTransformation(inPosition);
                normal = noTransformationNormal(inPosition);
                break;
        }
    } else if (obj == 1) {
        xyz = transformCover(inPosition);
        normal = transformCoverNormal(inPosition);
    } else if (obj == 2) {
        xyz = transformLightSource();
    } else {
        xyz = noTransformation(inPosition);
        normal = noTransformationNormal(inPosition);
    }
    color = xyz;
    gl_Position = proj * view * vec4(xyz, 1.0);

    light = lightPosition - xyz;
    NdotL = vec3(dot(normal, light));

    mat4 invView = inverse(view);
    vec3 eyePosition = vec3(invView[3][0], invView[3][1], invView[3][2]);

    viewDirection = eyePosition - xyz;

    texCoord = inPosition;

    depthTexCoord = lightMat * vec4(xyz, 1.0);
    depthTexCoord.xyz = (depthTexCoord.xyz + 1) / 2;

    intensity = dot(normalize(light), normalize(normal));
}