#version 150

in vec2 inPosition;

uniform mat4 mat;
uniform int transformation;
uniform float time;
uniform int coverMovement;

uniform int obj;

const float PI = 3.14159265359;

mat3 translate(float x, float y) {
    return mat3(
        1, 0, x,
        0, 1, y,
        0, 0, 1
    );
}
mat3 rotate(float angle) {
    return mat3(
        cos(angle), -sin(angle), 0,
        sin(angle), cos(angle), 0,
        0, 0, 1
    );
}
mat3 scale(float x, float y) {
    return mat3(
        x, 0, 0,
        0, y, 0,
        0, 0, 1
    );
}

vec3 transformCover() {
    vec3 transformed;
    if (coverMovement == 0) {
        transformed = vec3(inPosition, 0.0);
    } else if (coverMovement == 1) {
        transformed = vec3(inPosition, 1.0) * translate(-0.5, -0.5) * rotate(time) * translate(0.5, 0.5) * translate(0, sin(2 * time) * 2);
    }
    return vec3(transformed.x, transformed.y, 2.0);
}

vec3 scaleObjectWithNoOtherTransformation() {
    vec3 scaled = vec3(inPosition.x, inPosition.y, 1.0) * scale(5, 5) * translate(-2.5, -2.5);
    return vec3(scaled.xy, 0.0);
}

vec3 noTransformation() {
    return vec3(inPosition, 0.0);
}

void main() {
    vec3 xyz;
    if (obj == 0 && transformation == 8) {
        xyz = scaleObjectWithNoOtherTransformation();
    } else if (obj == 1) {
        xyz = transformCover();
    }
    gl_Position = mat * vec4(xyz, 1.0);
}