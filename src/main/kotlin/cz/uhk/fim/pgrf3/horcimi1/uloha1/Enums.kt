package cz.uhk.fim.pgrf3.horcimi1.uloha1

enum class Projection(val description: String) {
    PERSPECTIVE("Perspective"), ORTHOGONAL("Orthogonal")
}

enum class PolygonMode(val description: String) {
    FILL("GL_FILL"), LINES("GL_LINES"), POINTS("GL_POINTS")
}

enum class Primitives(val description: String) {
    TRIANGLES("GL_TRIANGLES"), TRIANGLE_STRIP("GL_TRIANGLE_STRIP")
}

enum class Transformation(val description: String) {
    NONE("No transformation"),
    CARTESIAN("Cartesian"),
    CARTESIAN_CUSTOM("Cartesian custom"),
    SPHERICAL("Spherical"),
    SPHERICAL_CUSTOM("Spherical custom"),
    CYLINDRICAL("Cylindrical"),
    CYLINDRICAL_CUSTOM("Cylindrical custom"),
    TIME("Modification in time"),
    SHADOW_MAP("Shadow maps"),
    LIGHTING("Lighting")
}

enum class Lighting(val description: String) {
    PER_VERTEX("Per vertex"), PER_PIXEL("Per pixel"), BLINN_PHONG("Blinn-Phong")
}

inline fun <reified T : Enum<T>> Enum<T>.next(): T {
    val next = ordinal + 1
    val values = enumValues<T>()
    return values.elementAt(next % values.size)
}