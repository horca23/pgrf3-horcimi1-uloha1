package cz.uhk.fim.pgrf3.horcimi1.uloha1

data class Grid(val vertexBuffer: List<Float>, val indexBuffer: List<Int>)

fun generateGridUsingTriangles(m: Int, n: Int): Grid {
    val vertexBuffer = n.flatMapFromIndex { j ->
        val y = j / (n - 1).toFloat()
        m.flatMapFromIndex { i ->
            val x = i / (n - 1).toFloat()
            listOf(x, y)
        }
    }
    val indexBuffer = n.flatMapFromIndex(endOffset = 1) { r ->
        m.flatMapFromIndex(endOffset = 1) { c ->
            listOf(
                    r * m + c,
                    r * m + c + 1,
                    r * m + c + m,
                    r * m + c + m,
                    r * m + c + 1,
                    r * m + c + m + 1
            )
        }
    }
    return Grid(vertexBuffer, indexBuffer)
}

fun generateGridUsingTriangleStrip(m: Int, n: Int): Grid {
    val vertexBuffer = n.flatMapFromIndex { j ->
        val y = j / (n - 1).toFloat()
        m.flatMapFromIndex { i ->
            val x = i / (n - 1).toFloat()
            listOf(x, y)
        }
    }
    val indexBuffer = n.flatMapFromIndex(endOffset = 1) { r ->
        (if (r > 0) listOf(r * n) else emptyList()) // degeneration - repeat first vertex
                .plus(m.flatMapFromIndex { c -> listOf((r * n) + c, ((r + 1) * n) + c) }
                        .plus(if (r < n - 2) listOf(((r + 1) * n) + m - 1) else emptyList())) // degeneration - repeat last vertex
    }
    return Grid(vertexBuffer, indexBuffer)
}

private inline fun <T> Int.flatMapFromIndex(endOffset: Int = 0, transform: (Int) -> Iterable<T>)
        = (0 until this - endOffset).flatMap(transform)