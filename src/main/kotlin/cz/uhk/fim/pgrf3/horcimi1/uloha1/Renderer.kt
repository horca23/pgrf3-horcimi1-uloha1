package cz.uhk.fim.pgrf3.horcimi1.uloha1

import com.jogamp.newt.event.KeyEvent
import com.jogamp.newt.event.KeyListener
import com.jogamp.newt.event.MouseEvent
import com.jogamp.newt.event.MouseListener
import com.jogamp.opengl.GL2
import com.jogamp.opengl.GL2GL3
import com.jogamp.opengl.GLAutoDrawable
import com.jogamp.opengl.GLEventListener
import oglutils.*
import transforms.*
import transforms.Vec3D
import transforms.Camera




const val CAM_SPEED = .5

class Renderer(
        private var width: Int,
        private var height: Int
) : GLEventListener, MouseListener, KeyListener {

    private var GRID_SIZE = 50
    private var reRenderGrid = false

    private lateinit var buffers: OGLBuffers
    private lateinit var textRenderer: OGLTextRenderer
    private lateinit var renderTarget: OGLRenderTarget
    private lateinit var textureViewer: OGLTexture2D.Viewer
    private lateinit var texture: OGLTexture2D

    private var projection = Projection.PERSPECTIVE
    private var projectionMat: Mat4 = getProj()

    private var polygonMode = PolygonMode.FILL

    private var primitives = Primitives.TRIANGLES
    private var switchPrimitives = false

    private var showNormals = false

    private var camera = Camera()
            .withPosition(Vec3D(7.0, 7.0, 4.0))
            .withAzimuth(Math.PI * 1.25)
            .withZenith(Math.PI * -.125)
            .withFirstPerson(false)
    private var mx = 0
    private var my = 0

    private var shaderProgram = 0
    private var locView = 0
    private var locProj = 0
    private var locTransformation = 0
    private var transformation = Transformation.CARTESIAN
    private var locObj = 0
    private var locLightMat = 0
    private var locTime = 0
    private var time = 0f
    private var locShowNormals = 0
    private var locCoverMovement = 0
    private var coverMovement = true

    private var locLighting = 0
    private var lighting = Lighting.BLINN_PHONG

    private var lightShaderProgram = 0
    private var lightProjectionMat = getOrthoMat()
    private val lightDefaultPosition = Vec3D(3.0, 3.0, 1.5)
    private var lightPosition = Camera()
            .withPosition(lightDefaultPosition)
            .addAzimuth(0.0)
            .addZenith(-1.5)
    private var locLightPosition = 0
    private var lightLocMat = 0
    private var lightLocObj = 0
    private var lightLocTransformation = 0
    private var lightLocTime = 0
    private var lightLocCoverMovement = 0

    private var lightMovement = true


    override fun init(drawable: GLAutoDrawable) {
        val gl = drawable.gl.gL2
        println("Init GL is ${gl.javaClass.name}")
        println("OpenGL version ${gl.glGetString(GL2.GL_VERSION)}")
        println("OpenGL vendor ${gl.glGetString(GL2.GL_VENDOR)}")
        println("OpenGL renderer ${gl.glGetString(GL2.GL_RENDERER)}")
        println("OpenGL extension ${gl.glGetString(GL2.GL_EXTENSIONS)}")

        OGLUtils.shaderCheck(gl)

        // ------------------------ viewer shader ------------------------
        shaderProgram = ShaderUtils.loadProgram(gl, "/shader.vert", "/shader.frag", null, null, null, null)

        locView = gl.glGetUniformLocation(shaderProgram, "view")
        locProj = gl.glGetUniformLocation(shaderProgram, "proj")
        locTransformation = gl.glGetUniformLocation(shaderProgram, "transformation")
        locTime = gl.glGetUniformLocation(shaderProgram, "time")
        locCoverMovement = gl.glGetUniformLocation(shaderProgram, "coverMovement")

        locLightPosition = gl.glGetUniformLocation(shaderProgram, "lightPosition")
        locLightMat = gl.glGetUniformLocation(shaderProgram, "lightMat")

        locObj = gl.glGetUniformLocation(shaderProgram, "obj")
        locShowNormals = gl.glGetUniformLocation(shaderProgram, "showNormals")

        locLighting = gl.glGetUniformLocation(shaderProgram, "lighting")
        // ------------------------ viewer shader ------------------------

        // ------------------------ light shader ------------------------
        lightShaderProgram = ShaderUtils.loadProgram(gl, "/light.vert", "/light.frag", null, null, null, null)

        lightLocMat = gl.glGetUniformLocation(lightShaderProgram, "mat")
        lightLocTransformation = gl.glGetUniformLocation(lightShaderProgram, "transformation")
        lightLocTime = gl.glGetUniformLocation(lightShaderProgram, "time")
        lightLocCoverMovement = gl.glGetUniformLocation(lightShaderProgram, "coverMovement")

        lightLocObj = gl.glGetUniformLocation(lightShaderProgram, "obj")
        // ------------------------ light shader ------------------------

        createBuffers(gl)

        textRenderer = OGLTextRenderer(gl, drawable.surfaceWidth, drawable.surfaceHeight)

        gl.glEnable(GL2GL3.GL_DEPTH_TEST)

        texture = OGLTexture2D(gl, "/texture.jpg")
        textureViewer = OGLTexture2D.Viewer(gl)

        renderTarget = OGLRenderTarget(gl, width, height)
    }

    override fun display(drawable: GLAutoDrawable) {
        val gl = drawable.gl.gL2GL3

        if (reRenderGrid) {
            createBuffers(gl)
            reRenderGrid = false
        }

        if (switchPrimitives) {
            primitives = primitives.next()
            createBuffers(gl)
            switchPrimitives = false
        }

        time += .01f
        if (transformation == Transformation.LIGHTING && lightMovement) {
            rotateLight(time)
        }

        renderFromLight(gl)
        renderFromViewer(gl)

        if (transformation == Transformation.SHADOW_MAP) {
            gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL)
            textureViewer.view(renderTarget.colorTexture, -1.0, 0.5, 0.5)
        }

        if (transformation == Transformation.SHADOW_MAP) {
            textRenderer.drawStr2D(25, 200, "[N] Cover moving: $coverMovement")
        } else if (transformation == Transformation.LIGHTING) {
            textRenderer.drawStr2D(25, 225, "[M] Light moving: $lightMovement")
            textRenderer.drawStr2D(25, 200, "[L] Lighting: ${lighting.description}")
        }
        textRenderer.drawStr2D(25, 175, "[C] Show normal: $showNormals")
        textRenderer.drawStr2D(25, 150, "[V] First person: ${camera.firstPerson}")
        textRenderer.drawStr2D(25, 125, "[F] Polygon mode: ${polygonMode.description}")
        textRenderer.drawStr2D(25, 100, "[P] Projection mode: ${projection.description}")
        textRenderer.drawStr2D(25, 75, "[Enter] Primitives: ${primitives.description}")
        textRenderer.drawStr2D(25, 50, "[UP][DOWN] Increase/decrease grid size ($GRID_SIZE)")
        textRenderer.drawStr2D(25, 25, buildTransformationLabel())
    }

    private fun renderFromLight(gl: GL2GL3) {
        gl.glUseProgram(lightShaderProgram)

        renderTarget.bind()

        gl.glClearColor(.1f, .1f, .1f, 1.0f)
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT or GL2GL3.GL_DEPTH_BUFFER_BIT)

        gl.glUniformMatrix4fv(lightLocMat, 1, false, lightPosition.viewMatrix.mul(lightProjectionMat).floatArray(), 0)
        gl.glUniform1i(lightLocTransformation, transformation.ordinal)
        gl.glUniform1f(lightLocTime, time)
        gl.glUniform1i(lightLocCoverMovement, coverMovement.toNumber())

        gl.glUniform1i(lightLocObj, 0)
        when (primitives) {
            Primitives.TRIANGLES -> buffers.draw(GL2GL3.GL_TRIANGLES, lightShaderProgram)
            Primitives.TRIANGLE_STRIP -> buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, lightShaderProgram)
        }

        if (transformation == Transformation.SHADOW_MAP) {
            gl.glUniform1i(lightLocObj, 1)
            when (primitives) {
                Primitives.TRIANGLES -> buffers.draw(GL2GL3.GL_TRIANGLES, lightShaderProgram)
                Primitives.TRIANGLE_STRIP -> buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, lightShaderProgram)
            }
        }
    }

    private fun renderFromViewer(gl: GL2GL3) {
        gl.glUseProgram(shaderProgram)

        gl.glBindFramebuffer(GL2GL3.GL_FRAMEBUFFER, 0)
        gl.glViewport(0, 0, width, height)

        gl.glClearColor(.1f, .1f, .1f, 1.0f)
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT or GL2GL3.GL_DEPTH_BUFFER_BIT)

        gl.glUniformMatrix4fv(locView, 1, false, camera.viewMatrix.floatArray(), 0)
        gl.glUniformMatrix4fv(locProj, 1, false, projectionMat.floatArray(), 0)
        gl.glUniform1i(locTransformation, transformation.ordinal)
        gl.glUniform1f(locTime, time)
        gl.glUniform1i(locCoverMovement, coverMovement.toNumber())

        gl.glUniform3fv(locLightPosition, 1, ToFloatArray.convert(lightPosition.position), 0)
        gl.glUniformMatrix4fv(locLightMat, 1, false, lightPosition.viewMatrix.mul(lightProjectionMat).floatArray(), 0)

        gl.glUniform1i(locShowNormals, showNormals.toNumber())
        gl.glUniform1i(locLighting, lighting.ordinal)

        texture.bind(shaderProgram, "textureID", 0)
        renderTarget.depthTexture.bind(shaderProgram, "depthTexture", 1)

        when (polygonMode) {
            PolygonMode.FILL -> gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL)
            PolygonMode.LINES -> gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_LINE)
            PolygonMode.POINTS -> gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_POINT)
        }

        // base grid
        gl.glUniform1i(locObj, 0)
        when (primitives) {
            Primitives.TRIANGLES -> buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram)
            Primitives.TRIANGLE_STRIP -> buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, shaderProgram)
        }

        // covering grid
        if (transformation == Transformation.SHADOW_MAP) {
            gl.glUniform1i(locObj, 1)
            when (primitives) {
                Primitives.TRIANGLES -> buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram)
                Primitives.TRIANGLE_STRIP -> buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, shaderProgram)
            }
        }

        // light source
        if (transformation == Transformation.LIGHTING) {
            gl.glUniform1i(locObj, 2)
            when (primitives) {
                Primitives.TRIANGLES -> buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram)
                Primitives.TRIANGLE_STRIP -> buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, shaderProgram)
            }
        }
    }

    override fun reshape(drawable: GLAutoDrawable, x: Int, y: Int, width: Int, height: Int) {
        this.width = width
        this.height = height
        projectionMat = getProj()
        lightProjectionMat = getOrthoMat()
    }

    override fun dispose(drawable: GLAutoDrawable) {
        val gl = drawable.gl.gL2GL3
        gl.glDeleteProgram(shaderProgram)
        gl.glDeleteProgram(lightShaderProgram)
    }

    private fun rotateLight(time: Float) {
        lightPosition = lightPosition.withPosition(
                lightDefaultPosition.mul(Mat3Rot2D(time.toDouble()))
        )
    }

    private fun getProj() = when (projection) {
        Projection.PERSPECTIVE -> getPerspMat()
        Projection.ORTHOGONAL -> getOrthoMat()
    }

    private fun getPerspMat() = Mat4PerspRH(Math.PI / 4, height / width.toDouble(), .01, 100.0)

    private fun getOrthoMat() = Mat4OrthoRH(7.5 / (height / width.toDouble()), 7.5, .01, 100.0)

    private fun createBuffers(gl: GL2GL3) {
        val (vb, ib) = when (primitives) {
            Primitives.TRIANGLES -> generateGridUsingTriangles(GRID_SIZE, GRID_SIZE)
            Primitives.TRIANGLE_STRIP -> generateGridUsingTriangleStrip(GRID_SIZE, GRID_SIZE)
        }
        buffers = OGLBuffers(gl, vb.toFloatArray(), arrayOf(OGLBuffers.Attrib("inPosition", 2)), ib.toIntArray())
    }

    override fun mousePressed(e: MouseEvent) {
        mx = e.x
        my = e.y
    }

    override fun mouseDragged(e: MouseEvent) {
        camera = camera.addAzimuth(Math.PI * (mx - e.x) / width.toDouble()).addZenith(-Math.PI * (e.y - my) / height.toDouble())
        mx = e.x
        my = e.y
    }

    override fun keyPressed(e: KeyEvent) {
        when (e.keyCode) {
            KeyEvent.VK_W -> camera = camera.forward(CAM_SPEED)
            KeyEvent.VK_S -> camera = camera.backward(CAM_SPEED)
            KeyEvent.VK_A -> camera = camera.left(CAM_SPEED)
            KeyEvent.VK_D -> camera = camera.right(CAM_SPEED)
            KeyEvent.VK_SHIFT -> camera = camera.up(CAM_SPEED)
            KeyEvent.VK_CONTROL -> camera = camera.down(CAM_SPEED)
            KeyEvent.VK_C -> showNormals = !showNormals
            KeyEvent.VK_P -> {
                projection = projection.next()
                projectionMat = getProj()
            }
            KeyEvent.VK_N -> {
                if (transformation == Transformation.SHADOW_MAP) {
                    coverMovement = !coverMovement
                }
            }
            KeyEvent.VK_M -> {
                if (transformation == Transformation.LIGHTING) {
                    lightMovement = !lightMovement
                }
            }
            KeyEvent.VK_F -> polygonMode = polygonMode.next()
            KeyEvent.VK_ENTER -> switchPrimitives = true
            KeyEvent.VK_V -> camera = camera.withFirstPerson(!camera.firstPerson)
            KeyEvent.VK_L -> {
                if (transformation == Transformation.LIGHTING) {
                    lighting = lighting.next()
                }
            }
            KeyEvent.VK_0, KeyEvent.VK_NUMPAD0 -> transformation = findTransformationForOrdinal(0)
            KeyEvent.VK_1, KeyEvent.VK_NUMPAD1 -> transformation = findTransformationForOrdinal(1)
            KeyEvent.VK_2, KeyEvent.VK_NUMPAD2 -> transformation = findTransformationForOrdinal(2)
            KeyEvent.VK_3, KeyEvent.VK_NUMPAD3 -> transformation = findTransformationForOrdinal(3)
            KeyEvent.VK_4, KeyEvent.VK_NUMPAD4 -> transformation = findTransformationForOrdinal(4)
            KeyEvent.VK_5, KeyEvent.VK_NUMPAD5 -> transformation = findTransformationForOrdinal(5)
            KeyEvent.VK_6, KeyEvent.VK_NUMPAD6 -> transformation = findTransformationForOrdinal(6)
            KeyEvent.VK_7, KeyEvent.VK_NUMPAD7 -> transformation = findTransformationForOrdinal(7)
            KeyEvent.VK_8, KeyEvent.VK_NUMPAD8 -> {
                lightPosition = lightPosition.withPosition(Vec3D(0.0, 0.0, 5.0))
                transformation = findTransformationForOrdinal(8)
            }
            KeyEvent.VK_9, KeyEvent.VK_NUMPAD9 -> {
                lightPosition = lightPosition.withPosition(lightDefaultPosition)
                transformation = findTransformationForOrdinal(9)
            }
            KeyEvent.VK_UP -> {
                GRID_SIZE = Math.min(500, GRID_SIZE + 10)
                reRenderGrid = true

            }
            KeyEvent.VK_DOWN -> {
                GRID_SIZE = Math.max(20, GRID_SIZE - 10)
                reRenderGrid = true
            }
        }
    }

    override fun mouseReleased(e: MouseEvent) { /*noop*/ }

    override fun mouseEntered(e: MouseEvent) { /*noop*/ }

    override fun mouseClicked(e: MouseEvent) { /*noop*/ }

    override fun mouseExited(e: MouseEvent) { /*noop*/ }

    override fun mouseMoved(e: MouseEvent) { /*noop*/ }

    override fun mouseWheelMoved(e: MouseEvent) { /*noop*/ }

    override fun keyReleased(e: KeyEvent) { /*noop*/ }

    private fun buildTransformationLabel(): String {
        var transformationLabel = ""
        Transformation.values().forEach {
            var label = "[${it.ordinal}] ${it.description}"
            if (it == transformation) {
                label = "<$label>"
            }
            transformationLabel += "$label "
        }
        return transformationLabel
    }

    private fun findTransformationForOrdinal(ordinal: Int): Transformation {
        return Transformation.values().first { it.ordinal == ordinal }
    }

    private fun Boolean.toNumber() = if (this) 1 else 0

}